
terraform {
    required_version = "~> 0.11.11"
    backend "gcs" {
         credentials = "./creds/serviceaccount.json"
         bucket      = "devfestab"
    }
}
