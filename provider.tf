provider "google" {
   version = "1.20.0"
   credentials = "${file("./creds/serviceaccount.json")}"
   project     = "devfestab" # REPLACE WITH YOUR PROJECT ID
   region      = "us-central1"
 }
