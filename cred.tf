
terraform {
    required_version  = ">= 0.12.7"
    required_providers {
        google = ">= 3.0"
    }
    backend "gcs" {
        bucket  = "terraform-state-betworks"
        prefix  = "ia1ahd1"
        credentials = "creds/terraform-admin-devfest.json"
    }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
  credentials = file(var.credential)
}

provider "google-beta" {
  version = ">= 2.9.0"
  project = var.project
  region  = var.region
  credentials = file(var.credential)
}
