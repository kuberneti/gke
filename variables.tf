variable "project_name"         { default = "devfestab" }
variable "region_name"          { default = "us-central1" }
variable "zone"                 { default = "us-central1-a" }
#GKE 
variable "cluster_name"         { default = "devfestab04" }
variable "node_pool_name"       { default = "devfestpool" }
variable "node_count"           { default = 1 }
variable "node_count_min"       { default = 1 }
variable "node_count_max"       { default = 2 }
variable "desired_gke_version"  { default = "1.17.13-gke.2001" }
variable "machine_type"         { default = "e2-micro" }
variable "node_disk_size_gb"    { default = 10 }
variable "enable_private_nodes" { default = true }
variable "disable_public_endpoint" { default = false }
